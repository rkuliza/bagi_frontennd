import styled from 'styled-components';
import Colors from '../../styles/colors';
import Spacing from '../../styles/spacing'
import GetQuoteCarImage from '../../images/get_quote.jpg';

export const CarRenewQuoteBodyStyle = styled.div`
    background-color: ${Colors.secondaryColor};
    padding: 0px 120px;
`;
export const RenewQuoteHeaderStyle = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    align-items: center;
    flex-direction: row;

    & .carRenewQuote-header-right {
        margin-right: 20%
    }
`;
export const RenewQuoteRegistrationStyle = styled.div`
    border-radius: 37px;
    background-color: ${Colors.whiteColor};
    padding: ${Spacing.p30};

    &.carRenew-registration-image {
        background-image: url(${GetQuoteCarImage});
        background-position-x: 100%;
        background-repeat: no-repeat;
        background-size: contain;
    }
    & .carRenewQuote-body-registration-left {
        width: 60%;
    }
    & .form-control:invalid {
        border-color: ${Colors.redColor};
        background-image: none;
    }
    & .form-control:invalid:focus {
        border-color:  ${Colors.redColor};
        box-shadow: none;
    }
    & .form-control:valid {
        border-color: ${Colors.blackColor};
        background-image: none;
    }
    & .form-control:valid:focus {
        border-color:  ${Colors.primaryColor};
        box-shadow: none;
    }
`;

