const Constants = {
    carInsurance : "Car Insurance",
    renewQuote : "Renew Quote",
    reasonsToBuy : "Reasons To Buy",
    whyUs : "Why Us?",
    enterYourPolicyNumberToGetYourPolicy : "Enter your policy number to get your policy back on track",
    idLikeToSwitchTBhartiAxaFromAnotherInsurer : "I'd like to switch to Bharti AXA from another insurer",
    carInsuranceRenewOthersLink : "car-insurance-renew-others",
    policyNumber : "Policy Number",
    carRegisterationNumber : "car Registration",
    policyExpiryDate : "Policy Expiry Date",
    renewNow : "Renew Now",
    pleaseProvidePolicyNumber : "Please provide correct policy number"
}
export default Constants;