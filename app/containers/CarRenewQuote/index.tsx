import React, { Component } from 'react';
import Constants from './constants';
import RenewQuoteHeader from './RenewQuoteHeader';
import RenewQuoteRegistration from './RenewQuoteRegistration';
import {CarRenewQuoteBodyStyle} from './styles';

export default class CarRenewQuote extends Component {
    render() {
        const { carInsurance,renewQuote,reasonsToBuy,whyUs } =  Constants;
        return (
            <CarRenewQuoteBodyStyle>
                <RenewQuoteHeader carInsurance={carInsurance} renewQuote={renewQuote} reasonsToBuy={reasonsToBuy} whyUs={whyUs}/>     
                <RenewQuoteRegistration/>       
            </CarRenewQuoteBodyStyle>
        )
    }
}
