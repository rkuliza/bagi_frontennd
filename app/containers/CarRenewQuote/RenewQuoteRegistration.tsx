import React, { Component } from 'react';
import Constants from './constants';
import {Link} from 'react-router-dom';
import {Form} from 'react-bootstrap';
import {RenewQuoteRegistrationStyle} from './styles';
import Input from '../../components/Input/index';
import ButtonComponent from '../../components/Button/index';


export default class RenewQuoteRegistration extends Component {
    constructor(props) {
        super(props);   
        this.submitRenewQuoteRegisterationForm = this.submitRenewQuoteRegisterationForm.bind(this);        
    }
    submitRenewQuoteRegisterationForm = e => {
        e.preventDefault();
        console.log(this.refs.name);
    }
    render() {
        const {enterYourPolicyNumberToGetYourPolicy, idLikeToSwitchTBhartiAxaFromAnotherInsurer,carInsuranceRenewOthersLink,renewNow,pleaseProvidePolicyNumber,policyNumber} = Constants;
        return (
            <RenewQuoteRegistrationStyle className="carRenew-registration-image">
                <div className="carRenewQuote-body-registration-left">
                    <h2>{enterYourPolicyNumberToGetYourPolicy}</h2>
                    <Link to={carInsuranceRenewOthersLink}>{idLikeToSwitchTBhartiAxaFromAnotherInsurer}</Link>
                    <div className="carRenewQuote-Registration-form">
                        <Form
                            noValidate
                            validated={true}
                            onSubmit={e => this.submitRenewQuoteRegisterationForm(e)}
                        >
                            <Form.Row>   
                                <Input 
                                    md="4" 
                                    name={policyNumber} 
                                    title={policyNumber} 
                                    className="aaaaa" 
                                    type="text" 
                                    placeholder="I0098965" 
                                    errorMessage = {pleaseProvidePolicyNumber}
                                />
                            </Form.Row>
                            <ButtonComponent type="submit" title={renewNow}/>
                        </Form>
                    </div>
                </div>
            </RenewQuoteRegistrationStyle>  
        )
    }    
}
