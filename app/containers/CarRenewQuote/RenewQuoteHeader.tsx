import React from 'react';
import {RenewQuoteHeaderStyle} from './styles';

const RenewQuoteHeader = (props) => {
    return (
        <RenewQuoteHeaderStyle>
            <div>
                <h1>
                    {props.carInsurance} : {props.renewQuote}
                </h1>                        
            </div>
            <div className="carRenewQuote-header-right">
                {props.reasonsToBuy} &nbsp; &nbsp; &nbsp;
                {props.whyUs}
            </div>
        </RenewQuoteHeaderStyle>  
    )
}
export default RenewQuoteHeader;
