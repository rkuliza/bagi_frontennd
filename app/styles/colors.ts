const Colors = {
    primaryColor : "#163e84",
    secondaryColor : "#f5f5f5",
    whiteColor: "#fff",
    blackColor: "#000000e6",
    lightBlueColor : "#00408521",
    redColor: "#ec1428"
}
export default Colors;