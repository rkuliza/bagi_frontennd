import React from 'react';
import {Form,Col} from 'react-bootstrap';

const Input = (props) => {
    return (
        <Form.Group as={Col} md={props.md} controlId={props.name}>             
            <Form.Label>{props.title}</Form.Label>
            <Form.Control
                required
                className={props.className}
                name={props.name}
                type={props.type}
                placeholder={props.placeholder} 
                as="input"
            />
            <Form.Control.Feedback type="invalid">{props.errorMessage}</Form.Control.Feedback>
        </Form.Group>
    )
}

export default Input;