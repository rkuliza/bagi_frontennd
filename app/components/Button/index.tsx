import React from 'react';
import {Button} from 'react-bootstrap';

const ButtonComponent = (props) => {
    return (
        <Button type={props.type}>{props.title}</Button>
    )
}

export default ButtonComponent;